#include <stdio.h>

void draw(const int size, char field[]);
void drawBorder(const int size);
int is_solved(const int size, char field[]);
int add_cross(const int size, char field[], const int position);

int main(){
    int size = -1, pos = -1;
    char player = 'B';
	
    printf("Enter the size of field: ");
    scanf("%d", &size);
	
    // create the board based on the given size
    // initialise the board with SPACEs
    char board[size];
    for (int i = 0; i < size; i++) board[i] = ' ';
    draw(size, board); 	// draw the board
	
    while (is_solved(size, board) != 1){
        player = player != 'A' ? 'A' : 'B';
		
        // let the player take turn to enter a position of the board
        // if the position is out of range or is occupied, print out a alert msg
        printf("Player %c: ", player);
        scanf("%d", &pos);

        // redraw the board if it is successfully added 
        if (add_cross(size, board, pos) == 1){
            draw(size, board);
        }
    }
    printf("Player %c won!\n", player);
    return 0;
}

/*
 * Function: draw
 * ----------------------------
 * Draws the current board.
 */
void draw(const int size, char field[]){
    int i = -1;
    printf("\n");
    drawBorder(size);
	
    // draw the values from the array
    for (i = 0; i < size; i++) printf("|%c", field[i]);
    printf("|\n");  // the end of the board
    drawBorder(size);
	
    // draw the number labels
    for (i = 1; i <= size; i++) printf(" %d", i);
    printf("\n");
}

/*
 * Function: drawBorder
 * ----------------------------
 * Draws the border line of the board.
 */
void drawBorder(const int size){
    for (int i = 0; i < size; ++i) printf("+-");
    printf("+\n");
}

/*
 * Function: add_cross
 * ----------------------------
 * Add a cross to the board based on the given position. The given position must
 * within the index range of the board. If the move is invalid, a message will print out.
 *
 * returns: -1 if the given position is out of bound, 1 if successfully added to the board,
 * 	    or 0 if the position is occupied
 */
int add_cross(const int size, char field[], const int position){
    if (position-1 < 0 || position-1 >= size){
        printf("Wrong position!\n");
        return -1;
    } else if (field[position-1] != 'X'){
        field[position-1] = 'X';
        return 1;
    } else {
        printf("X is already there!\n");
        return 0;
    }
}

/*
 * Function: is_solved
 * ----------------------------
 * To check whether the current game is already solved or not.
 * The board must contains three 'X' in a row for winning.
 *
 * returns: 1 if the board is solved, otherwise 0.
 */
int is_solved(const int size, char field[]){
    int count = 0;
    for (int i = 0; i < size; i++){
        if (field[i] == 'X') count++;
        else count = 0;
		
        if (count >= 3) return 1;
    }
    return 0;
}
